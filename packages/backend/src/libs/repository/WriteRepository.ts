import { Aggregate } from "@libs/domain/Aggregate";
import { UUIDv4 } from "@libs/domain/UUIDv4";
import { Option } from "oxide.ts";

export interface WriteRepository<T extends Aggregate> {
	getById(id: UUIDv4): Promise<Option<T>>;
	save(aggregate: T): Promise<unknown>;
}
