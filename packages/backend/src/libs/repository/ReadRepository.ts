import { Option } from "oxide.ts";

export interface ReadRepository<T> {
	getById(id: string): Promise<Option<T>>;
	getAll(): Promise<T[]>;
}
