import { randomUUID } from "crypto";

export type QueryProps<T> = Omit<T, "id" | "createdAt">;

export abstract class Query {
	readonly id: string;
	readonly createdAt: number;

	constructor() {
		this.id = randomUUID();
		this.createdAt = Date.now();
	}
}
