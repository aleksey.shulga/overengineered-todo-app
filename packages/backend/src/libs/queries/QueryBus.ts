import { Query } from "./Query";
import { QueryHandler } from "./QueryHandler";

export interface QueryBus {
	registerHandler<T extends Query, R>(
		type: typeof Query,
		handler: QueryHandler<T, R>,
	): void;
	dispatch<TQueryResult>(query: Query): Promise<TQueryResult>;
}
