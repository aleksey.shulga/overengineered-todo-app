import { Query } from "./Query";
import { QueryBus } from "./QueryBus";
import { QueryHandler } from "./QueryHandler";

export class SimpleQueryBus implements QueryBus {
	private handlers: Map<string, QueryHandler<Query, unknown>> = new Map();

	registerHandler<T extends Query, R>(
		type: typeof Query,
		handler: QueryHandler<T, R>,
	): void {
		this.handlers.set(type.name, handler);
	}

	async dispatch<TQueryResult>(query: Query): Promise<TQueryResult> {
		const handler = this.handlers.get(query.constructor.name) as QueryHandler<
			Query,
			TQueryResult
		>;
		if (!handler) {
			throw new Error(
				`No handler registered for query ${query.constructor.name}`,
			);
		}
		return handler.handle(query);
	}
}
