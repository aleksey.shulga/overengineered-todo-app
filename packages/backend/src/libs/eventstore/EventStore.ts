import { DomainEvent } from "@libs/domain/DomainEvent";
import { Result } from "oxide.ts";
import { ConcurrencyError } from "./errors/ConcurrencyError";

export interface EventStore {
	save(
		streamName: string,
		eventOrEvents: DomainEvent | DomainEvent[],
		expectedRevision: number,
	): Promise<Result<void, ConcurrencyError>>;
	read(streamName: string): Promise<DomainEvent[]>;
}
