/**
 * Error thrown when save event to event store fails due to concurrency issues.
 */
export class ConcurrencyError {
	constructor(
		public readonly expectedRevision: bigint,
		public readonly actualRevision: bigint,
	) {}
}
