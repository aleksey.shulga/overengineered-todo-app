import { Command } from "./Command";
import { CommandHandler } from "./CommandHandler";

export interface CommandBus {
	registerHandler<TCommand extends Command, TResult>(
		type: typeof Command,
		handler: CommandHandler<TCommand, TResult>,
	): void;
	execute<T>(command: Command): Promise<T>;
}
