import { randomUUID } from "crypto";

export type CommandProps<T> = Omit<T, "id" | "createdAt">;

export abstract class Command {
	readonly id: string;
	readonly createdAt: number;

	constructor() {
		this.id = randomUUID();
		this.createdAt = Date.now();
	}
}
