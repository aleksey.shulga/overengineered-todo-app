import { Command } from "./Command";
import { CommandBus } from "./CommandBus";
import { CommandHandler } from "./CommandHandler";

export class SimpleCommandBus implements CommandBus {
	private handlers: Map<string, CommandHandler<Command, unknown>> = new Map();

	registerHandler<TCommand extends Command, TResult>(
		type: typeof Command,
		handler: CommandHandler<TCommand, TResult>,
	): void {
		this.handlers.set(type.name, handler);
	}

	async execute<T>(command: Command): Promise<T> {
		const handler = this.handlers.get(
			command.constructor.name,
		) as CommandHandler<Command, T>;
		if (!handler) {
			throw new Error(
				`No handler registered for command ${command.constructor.name}`,
			);
		}
		return handler.handle(command);
	}
}
