import { DomainError } from "./DomainError";
import { DomainRule } from "./DomainRule";

export const applyRules = (rules: DomainRule[]): DomainError[] => {
	const errors: DomainError[] = [];
	for (const rule of rules) {
		const error = rule.checkIfBroken();
		if (error !== null) {
			errors.push(error);
		}
	}
	return errors;
};
