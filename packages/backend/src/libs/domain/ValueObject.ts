export type ValueObjectProps = Record<string, unknown>;

export abstract class ValueObject<T extends ValueObjectProps> {
	protected readonly props: T;

	constructor(props: T) {
		this.props = Object.freeze(props);
	}

	public equals(valueObject: ValueObject<T>): boolean {
		return JSON.stringify(this.props) === JSON.stringify(valueObject.props);
	}
}
