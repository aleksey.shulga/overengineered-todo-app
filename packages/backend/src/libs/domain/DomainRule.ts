import { DomainError } from "./DomainError";

export interface DomainRule {
	checkIfBroken(): DomainError | null;
}
