import { randomUUID } from "crypto";
import { ValueObject } from "./ValueObject";

type UUIDv4Props = {
	value: string;
};

export class UUIDv4 extends ValueObject<UUIDv4Props> {
	constructor(id?: string) {
		super({ value: id ? id : randomUUID() });
	}

	toString(): string {
		return this.props.value;
	}
}
