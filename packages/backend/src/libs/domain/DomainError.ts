export abstract class DomainError {
	constructor(public readonly message: string, public readonly cause?: Error) {}
}
