import { DomainEvent } from "./DomainEvent";
import { Entity } from "./Entity";
import { UUIDv4 } from "./UUIDv4";

export interface AggregateType<T extends Aggregate> {
	new (id?: UUIDv4): T;
}

export interface AggregateProps {
	id?: UUIDv4;
}

export abstract class Aggregate<
	TProps extends AggregateProps = AggregateProps,
> extends Entity {
	private readonly events: DomainEvent[] = [];
	protected readonly props: TProps;
	protected version = 0;

	protected constructor(props: TProps) {
		super(props.id);
		this.props = props;
	}

	public pullEvents(): DomainEvent[] {
		const events = [...this.events];
		this.events.splice(0, this.events.length);
		return events;
	}

	protected addEvent(domainEvent: DomainEvent): void {
		this.events.push(domainEvent);
	}

	public getVersion(): number {
		return this.version;
	}
}
