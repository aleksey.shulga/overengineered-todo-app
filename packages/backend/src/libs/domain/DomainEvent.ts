import { randomUUID } from "crypto";

export type DomainEventProps<T> = Omit<T, "id" | "createdAt"> & {
	aggregateId: string;
};

export abstract class DomainEvent {
	readonly id: string;
	readonly createdAt: number;
	readonly aggregateId: string;

	constructor(props: DomainEventProps<unknown>) {
		this.id = randomUUID();
		this.createdAt = Date.now();
		this.aggregateId = props.aggregateId;
	}
}
