import { UUIDv4 } from "./UUIDv4";

export abstract class Entity {
	public readonly id: UUIDv4;

	constructor(id?: UUIDv4) {
		this.id = id ? id : new UUIDv4();
	}

	public equals(object: Entity): boolean {
		return this.id === object.id;
	}
}
