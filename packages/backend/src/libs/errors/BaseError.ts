export abstract class BaseError {
	constructor(public readonly message: string, public readonly cause?: Error) {}
}
