import * as path from "path";
import { config } from "dotenv";

const env = process.env.NODE_ENV;

const envPath: string = path.resolve(
	__dirname,
	env ? `../../../.env.${env}` : "../../../../.env",
);

export const loadDotEnv = () => {
	config({ path: envPath });
};
