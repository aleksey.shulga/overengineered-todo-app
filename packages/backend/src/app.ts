import { Hono } from "hono";

import { loadDotEnv } from "@libs/utils";
import { createAuthRoutes } from "modules/auth/application/http/routes";
import { createTodoRoutes } from "modules/todo/application/http/routes";
import { SimpleCommandBus } from "./libs/commands/SimpleCommandBus";
import { SimpleQueryBus } from "./libs/queries/SimpleQueryBus";

loadDotEnv();

const app = new Hono();

const commandBus = new SimpleCommandBus();
const queryBus = new SimpleQueryBus();

app.route("/auth/", createAuthRoutes(commandBus));
app.route("/todos", createTodoRoutes(commandBus));

export default app;
