import { CommandBus } from "@libs/commands";
import { Hono } from "hono";
import { HTTPException } from "hono/http-exception";
import { LoginUserCommand } from "modules/auth/commands/LoginUserCommand";
import { match } from "oxide.ts";
import { LoginUserCommandResult } from "../command-handlers/LoginUserCommandHandler";

export const createAuthRoutes = (commandBus: CommandBus): Hono => {
	const router = new Hono();

	router.post("/login", async (c) => {
		const { email, password } = await c.req.json();
		const command = new LoginUserCommand({ email, password });
		const result = await commandBus.execute<LoginUserCommandResult>(command);
		return match(result, {
			Ok: () => {
				c.status(200);
			},
			Err: (error) => {
				throw new HTTPException(401, { message: error.message });
			},
		});
	});

	return router;
};
