import { CommandHandler } from "@libs/commands";
import { UUIDv4 } from "@libs/domain";
import bcrypt from "bcrypt";
import { UserReadRepository } from "modules/auth/repository/UserReadRepository";
import { Err, Ok, Result } from "oxide.ts";
import { LoginUserCommand } from "../../commands/LoginUserCommand";
import { UserWriteRepository } from "../../repository/UserWriteRepository";
import { AuthenticationError } from "../errors/AuthenticationError";

export type LoginUserCommandResult = Result<UUIDv4, AuthenticationError>;

export class LoginUserCommandHandler
	implements CommandHandler<LoginUserCommand, LoginUserCommandResult>
{
	constructor(
		private readonly userReadRepo: UserReadRepository,
		private readonly userWriteRepo: UserWriteRepository,
	) {}

	async handle(command: LoginUserCommand): Promise<LoginUserCommandResult> {
		const userModelResult = await this.userReadRepo.getByEmail(command.email);
		if (userModelResult.isNone()) {
			return Err(new AuthenticationError());
		}
		const userModel = userModelResult.unwrap();
		const userId = new UUIDv4(userModel.id);
		const userEntityResult = await this.userWriteRepo.getById(userId);
		if (userEntityResult.isNone()) {
			return Err(new AuthenticationError());
		}
		const userEntity = userEntityResult.unwrap();

		const passwordMatch = await bcrypt.compare(
			command.password,
			userEntity.password,
		);
		if (!passwordMatch) {
			return Err(new AuthenticationError());
		}
		return Ok(userId);
	}
}
