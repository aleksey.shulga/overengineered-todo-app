import { BaseError } from "@libs/errors";

export class AuthenticationError extends BaseError {
	constructor(cause?: Error) {
		super("Invalid credentials", cause);
	}
}
