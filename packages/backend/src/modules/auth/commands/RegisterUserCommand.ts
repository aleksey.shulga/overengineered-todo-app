import { Command, CommandProps } from "@libs/commands";

export class RegisterUserCommand extends Command {
	readonly email: string;
	readonly password: string;

	constructor(props: CommandProps<RegisterUserCommand>) {
		super();
		this.email = props.email;
		this.password = props.password;
	}
}
