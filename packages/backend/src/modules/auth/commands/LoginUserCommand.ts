import { Command, CommandProps } from "@libs/commands";

export class LoginUserCommand extends Command {
	readonly email: string;
	readonly password: string;

	constructor(props: CommandProps<LoginUserCommand>) {
		super();
		this.email = props.email;
		this.password = props.password;
	}
}
