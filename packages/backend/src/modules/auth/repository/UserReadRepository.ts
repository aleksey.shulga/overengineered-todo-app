import { ReadRepository } from "@libs/repository";
import { Option } from "oxide.ts";
import { UserModel } from "../models/UserModel";

export class UserReadRepository implements ReadRepository<UserModel> {
	async getById(id: string): Promise<Option<UserModel>> {
		throw new Error("Method not implemented.");
	}

	async getAll(): Promise<UserModel[]> {
		throw new Error("Method not implemented.");
	}

	async getByEmail(email: string): Promise<Option<UserModel>> {
		throw new Error("Method not implemented.");
	}
}
