import { UUIDv4 } from "@libs/domain";
import { ConcurrencyError, EventStore } from "@libs/eventstore";
import { WriteRepository } from "@libs/repository";
import { None, Option, Result, Some } from "oxide.ts";
import { UserEntity } from "../domain/UserEntity";

export class UserWriteRepository implements WriteRepository<UserEntity> {
	constructor(private eventStore: EventStore) {}
	async getById(id: UUIDv4): Promise<Option<UserEntity>> {
		const streamName = this.getStreamName(id);
		const events = await this.eventStore.read(streamName);
		if (events.length === 0) {
			return None;
		}
		const user = UserEntity.fromEvents(events);
		return Some(user);
	}

	async save(user: UserEntity): Promise<Result<void, ConcurrencyError>> {
		const events = user.pullEvents();
		const srteamName = this.getStreamName(user.id);
		return await this.eventStore.save(srteamName, events, user.getVersion());
	}

	private getStreamName(id: UUIDv4): string {
		return `user-${id}`;
	}
}
