import { Aggregate, DomainError, DomainEvent, UUIDv4 } from "@libs/domain";
import { Ok, Result } from "oxide.ts";
import { EmailValueObject } from "./EmailValueObject";
import { UserLoggedInDomainEvent } from "./events/UserLoggedInDomainEvent";
import { UserRegisteredDomainEvent } from "./events/UserRegisteredDomainEvent";

interface UserProps {
	id?: UUIDv4;
	email: EmailValueObject;
	password: string;
	lastLogin?: Date;
}

export class UserEntity extends Aggregate<UserProps> {
	get email(): EmailValueObject {
		return this.props.email;
	}

	get password(): string {
		return this.props.password;
	}

	get lastLogin(): Date | undefined {
		return this.props.lastLogin;
	}

	static register(props: UserProps): Result<UserEntity, DomainError[]> {
		const user = new UserEntity(props);
		const event = new UserRegisteredDomainEvent({
			aggregateId: user.id.toString(),
			email: user.email.toString(),
			password: user.password,
		});
		user.addEvent(event);
		user.version++;
		return Ok(user);
	}

	static fromEvents(events: DomainEvent[]): UserEntity {
		let primitiveId = "";
		let primitiveEmail = "";
		let primitivePassword = "";
		let primitiveLastLogin = new Date();
		for (const domainEvent of events) {
			if (domainEvent instanceof UserRegisteredDomainEvent) {
				primitiveId = domainEvent.aggregateId;
				primitiveEmail = domainEvent.email;
				primitivePassword = domainEvent.password;
			}
			if (domainEvent instanceof UserLoggedInDomainEvent) {
				primitiveLastLogin = domainEvent.date;
			}
		}
		return new UserEntity({
			id: new UUIDv4(primitiveId),
			email: EmailValueObject.create(primitiveEmail).unwrap(),
			password: primitivePassword,
			lastLogin: primitiveLastLogin,
		});
	}

	login(): Result<UserEntity, DomainError[]> {
		this.props.lastLogin = new Date();
		const event = new UserLoggedInDomainEvent({
			aggregateId: this.id.toString(),
			email: this.email.toString(),
			date: this.props.lastLogin,
		});
		this.addEvent(event);
		this.version++;
		return Ok(this);
	}
}
