import { DomainError, DomainRule } from "@libs/domain";
import { InvalidEmailError } from "../errors/InvalidEmailError";

const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

export class ValidEmailRule implements DomainRule {
	constructor(private readonly email: string) {}

	checkIfBroken(): DomainError | null {
		if (typeof this.email !== "string" || !emailRegex.test(this.email)) {
			return new InvalidEmailError(this.email);
		}
		return null;
	}
}
