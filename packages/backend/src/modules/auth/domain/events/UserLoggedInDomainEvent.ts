import { DomainEvent, DomainEventProps } from "@libs/domain";

export class UserLoggedInDomainEvent extends DomainEvent {
	readonly email: string;
	readonly date: Date;

	constructor(props: DomainEventProps<UserLoggedInDomainEvent>) {
		super(props);
		this.email = props.email;
		this.date = props.date;
	}
}
