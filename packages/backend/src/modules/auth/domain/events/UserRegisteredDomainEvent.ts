import { DomainEvent, DomainEventProps } from "@libs/domain";

export class UserRegisteredDomainEvent extends DomainEvent {
	readonly email: string;
	readonly password: string;
	constructor(props: DomainEventProps<UserRegisteredDomainEvent>) {
		super(props);
		this.email = props.email;
		this.password = props.password;
	}
}
