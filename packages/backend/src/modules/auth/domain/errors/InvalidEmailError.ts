import { DomainError } from "@libs/domain";

export class InvalidEmailError extends DomainError {
	constructor(email: string) {
		super(`Email '${email}' is invalid.`);
	}
}
