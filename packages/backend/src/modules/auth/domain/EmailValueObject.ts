import { DomainError, ValueObject, applyRules } from "@libs/domain";
import { Err, Ok, Result } from "oxide.ts";
import { ValidEmailRule } from "./rules/ValidEmailRule";

type EmailProps = {
	value: string;
};

export class EmailValueObject extends ValueObject<EmailProps> {
	get value(): string {
		return this.props.value;
	}

	private constructor(email: string) {
		super({ value: email });
	}

	static create(email: string): Result<EmailValueObject, DomainError[]> {
		const errors = applyRules([new ValidEmailRule(email)]);
		if (errors.length > 0) {
			return Err(errors);
		}

		return Ok(new EmailValueObject(email));
	}
}
