import { DomainError, DomainRule } from "@libs/domain";
import { TodoAlreadyUncompletedError } from "../errors/TodoAlreadyUncompletedError";

export class TodoUncompleteRule implements DomainRule {
	constructor(
		private readonly todoId: string,
		private readonly completed: boolean,
	) {}
	checkIfBroken(): DomainError | null {
		if (!this.completed) {
			return new TodoAlreadyUncompletedError(this.todoId);
		}
		return null;
	}
}
