import { DomainError, DomainRule } from "@libs/domain";
import { TodoAlreadyCompletedError } from "../errors/TodoAlreadyCompletedError";

export class TodoCompleteRule implements DomainRule {
	constructor(
		private readonly todoId: string,
		private readonly completed: boolean,
	) {}
	checkIfBroken(): DomainError | null {
		if (this.completed) {
			return new TodoAlreadyCompletedError(this.todoId);
		}
		return null;
	}
}
