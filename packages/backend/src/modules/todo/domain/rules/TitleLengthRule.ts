import { DomainError, DomainRule } from "@libs/domain";
import { InvalidTitleLenghtError as InvalidTitleError } from "../errors/InvalidTitleLenghtError";

export class TitleLengthRule implements DomainRule {
	constructor(private readonly title: string) {}
	checkIfBroken(): DomainError | null {
		if (
			typeof this.title !== "string" ||
			this.title.length > 120 ||
			this.title.length < 3
		) {
			return new InvalidTitleError(this.title);
		}
		return null;
	}
}
