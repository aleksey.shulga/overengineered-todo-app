import { DomainError, ValueObject, applyRules } from "@libs/domain";
import { Err, Ok, Result } from "oxide.ts";
import { TitleLengthRule } from "./rules/TitleLengthRule";

type TitleProps = {
	value: string;
};

export class TitleValueObject extends ValueObject<TitleProps> {
	get value(): string {
		return this.props.value;
	}

	private constructor(title: string) {
		super({ value: title });
	}

	static create(title: string): Result<TitleValueObject, DomainError[]> {
		const errors = applyRules([new TitleLengthRule(title)]);
		if (errors.length > 0) {
			return Err(errors);
		}

		return Ok(new TitleValueObject(title));
	}
}
