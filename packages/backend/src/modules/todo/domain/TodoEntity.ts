import {
	Aggregate,
	AggregateProps,
	DomainError,
	DomainEvent,
	UUIDv4,
	applyRules,
} from "@libs/domain";
import { Err, Ok, Result } from "oxide.ts";
import { TitleValueObject } from "./TitleValueObject";
import { TodoAddedDomainEvent } from "./events/TodoAddedDomainEvent";
import { TodoCompletedDomainEvent } from "./events/TodoCompletedDomainEvent";
import { TodoDeletedDomainEvent } from "./events/TodoDeletedDomainEvent";
import { TodoTitleChangedDomainEvent } from "./events/TodoTitleChangedDomainEvent";
import { TodoUncompletedDomainEvent } from "./events/TodoUncompletedDomainEvent";
import { TodoCompleteRule } from "./rules/TodoCompleteRule";
import { TodoUncompleteRule } from "./rules/TodoUncompleteRule";

interface TodoProps extends AggregateProps {
	userId: UUIDv4;
	title: TitleValueObject;
	completed: boolean;
	deleted?: boolean;
}

export class TodoEntity extends Aggregate<TodoProps> {
	get userId(): UUIDv4 {
		return this.props.userId;
	}

	get title(): TitleValueObject {
		return this.props.title;
	}

	get completed(): boolean {
		return this.props.completed;
	}

	get deleted(): boolean {
		return this.props.deleted ?? false;
	}

	static create(props: TodoProps): TodoEntity {
		const todo = new TodoEntity(props);

		const todoAddedDomainEvent = new TodoAddedDomainEvent({
			aggregateId: todo.id.toString(),
			title: todo.title.toString(),
			userId: todo.userId.toString(),
			completed: todo.completed,
		});
		todo.version++;
		todo.addEvent(todoAddedDomainEvent);

		return todo;
	}

	static fromEvents(events: DomainEvent[]): TodoEntity {
		let primitiveId = "";
		let primitiveUserId = "";
		let primitiveTitle = "";
		let completed = false;
		let deleted = false;

		for (const domainEvent of events) {
			if (domainEvent instanceof TodoAddedDomainEvent) {
				primitiveId = domainEvent.aggregateId;
				primitiveUserId = domainEvent.userId;
				primitiveTitle = domainEvent.title;
				completed = domainEvent.completed;
			}
			if (domainEvent instanceof TodoTitleChangedDomainEvent) {
				primitiveTitle = domainEvent.title;
			}
			if (domainEvent instanceof TodoCompletedDomainEvent) {
				completed = true;
			}
			if (domainEvent instanceof TodoUncompletedDomainEvent) {
				completed = false;
			}
			if (domainEvent instanceof TodoDeletedDomainEvent) {
				deleted = true;
			}
		}

		const id = new UUIDv4(primitiveId);
		const userId = new UUIDv4(primitiveUserId);
		const title = TitleValueObject.create(primitiveTitle).unwrap();
		const todo = new TodoEntity({ id, userId, title, completed, deleted });
		todo.version = events.length;
		return todo;
	}

	complete(): Result<TodoEntity, DomainError[]> {
		const errors = applyRules([
			new TodoCompleteRule(this.id.toString(), this.completed),
		]);
		if (errors.length > 0) {
			return Err(errors);
		}

		this.props.completed = true;
		const event = new TodoCompletedDomainEvent({
			aggregateId: this.id.toString(),
			title: this.title.toString(),
			userId: this.userId.toString(),
			completed: this.completed,
		});
		this.addEvent(event);
		this.version++;
		return Ok(this);
	}

	uncomplete(): Result<TodoEntity, DomainError[]> {
		const errors = applyRules([
			new TodoUncompleteRule(this.id.toString(), this.completed),
		]);
		if (errors.length > 0) {
			return Err(errors);
		}

		this.props.completed = false;
		const event = new TodoUncompletedDomainEvent({
			aggregateId: this.id.toString(),
			title: this.title.toString(),
			userId: this.userId.toString(),
			completed: this.completed,
		});
		this.addEvent(event);
		this.version++;
		return Ok(this);
	}

	changeTitle(title: TitleValueObject): Result<TodoEntity, DomainError[]> {
		this.props.title = title;
		const event = new TodoTitleChangedDomainEvent({
			aggregateId: this.id.toString(),
			title: this.title.toString(),
			userId: this.userId.toString(),
			completed: this.completed,
		});
		this.addEvent(event);
		this.version++;
		return Ok(this);
	}

	delete(): Result<TodoEntity, DomainError[]> {
		this.props.deleted = true;
		const event = new TodoDeletedDomainEvent({
			aggregateId: this.id.toString(),
			title: this.title.toString(),
			userId: this.userId.toString(),
			completed: this.completed,
		});
		this.addEvent(event);
		this.version++;
		return Ok(this);
	}
}
