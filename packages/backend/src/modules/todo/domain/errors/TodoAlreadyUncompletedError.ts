import { DomainError } from "@libs/domain";

export class TodoAlreadyUncompletedError extends DomainError {
	constructor(todoId: string) {
		super(`Todo with id '${todoId}' is already uncompleted`);
	}
}
