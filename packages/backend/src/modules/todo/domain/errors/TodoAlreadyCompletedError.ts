import { DomainError } from "@libs/domain";

export class TodoAlreadyCompletedError extends DomainError {
	constructor(todoId: string) {
		super(`Todo with id '${todoId}' is already completed`);
	}
}
