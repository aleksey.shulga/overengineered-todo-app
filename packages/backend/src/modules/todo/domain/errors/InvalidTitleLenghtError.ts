import { DomainError } from "@libs/domain";

export class InvalidTitleLenghtError extends DomainError {
	constructor(title: string) {
		super(
			`Title '${title}' is invalid. It must be between 3 and 120 characters long.`,
		);
	}
}
