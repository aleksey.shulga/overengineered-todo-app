import { DomainEvent, DomainEventProps } from "@libs/domain";

export class TodoCompletedDomainEvent extends DomainEvent {
	readonly title: string;
	readonly userId: string;
	readonly completed: boolean;

	constructor(props: DomainEventProps<TodoCompletedDomainEvent>) {
		super(props);
		this.title = props.title;
		this.userId = props.userId;
		this.completed = props.completed;
	}
}
