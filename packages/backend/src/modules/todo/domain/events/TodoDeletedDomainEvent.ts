import { DomainEvent, DomainEventProps } from "@libs/domain";

export class TodoDeletedDomainEvent extends DomainEvent {
	readonly title: string;
	readonly userId: string;
	readonly completed: boolean;

	constructor(props: DomainEventProps<TodoDeletedDomainEvent>) {
		super(props);
		this.title = props.title;
		this.userId = props.userId;
		this.completed = props.completed;
	}
}
