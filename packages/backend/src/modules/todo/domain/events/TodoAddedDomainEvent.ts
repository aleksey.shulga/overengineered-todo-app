import { DomainEvent, DomainEventProps } from "@libs/domain";

export class TodoAddedDomainEvent extends DomainEvent {
	readonly title: string;
	readonly userId: string;
	readonly completed: boolean;

	constructor(props: DomainEventProps<TodoAddedDomainEvent>) {
		super(props);
		this.title = props.title;
		this.userId = props.userId;
		this.completed = props.completed;
	}
}
