import { DomainEvent, DomainEventProps } from "@libs/domain";

export class TodoUncompletedDomainEvent extends DomainEvent {
	readonly title: string;
	readonly userId: string;
	readonly completed: boolean;

	constructor(props: DomainEventProps<TodoUncompletedDomainEvent>) {
		super(props);
		this.title = props.title;
		this.userId = props.userId;
		this.completed = props.completed;
	}
}
