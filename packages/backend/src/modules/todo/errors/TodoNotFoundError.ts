import { BaseError } from "@libs/errors";

export class TodoNotFoundError extends BaseError {
	constructor(id: string) {
		super(`Todo with id ${id} not found`);
	}
}
