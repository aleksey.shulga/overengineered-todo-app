import { QueryHandler } from "@libs/queries";
import { TodoModel } from "modules/todo/models/TodoModel";
import { TodoReadRepository } from "modules/todo/repository/TodoReadRepository";
import { GetTodosQuery } from "../../queries/GetTodosQuery";

export class GetTodosQueryHandler
	implements QueryHandler<GetTodosQuery, TodoModel[]>
{
	constructor(private readonly repo: TodoReadRepository) {}
	handle(query: GetTodosQuery): Promise<TodoModel[]> {
		return this.repo.getByUserId(query.userId);
	}
}
