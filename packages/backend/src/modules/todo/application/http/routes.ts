import { CommandBus } from "@libs/commands";
import { Hono } from "hono";
import { AddTodoCommand } from "modules/todo/commands/AddTodoCommand";
import { AddTodoCommandResult } from "../command-handlers/AddTodoCommandHandler";

export const createTodoRoutes = (commandBus: CommandBus): Hono => {
	const router = new Hono();

	router.post("/", async (c) => {
		const { title } = await c.req.json();
		const userId = c.get("jwtPayload").userId;
		const command = new AddTodoCommand({ userId, title });
		const result = await commandBus.execute<AddTodoCommandResult>(command);
		if (result.isErr()) {
			return c.json({ errors: result.unwrapErr() }, 400);
		}
		return c.json({ id: result.unwrap() }, 201);
	});

	return router;
};
