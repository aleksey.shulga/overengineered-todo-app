import { TodoNotFoundError } from "modules/todo/errors/TodoNotFoundError";
import { Err, Ok, Result } from "oxide.ts";

import { CommandHandler } from "@libs/commands";
import { DomainError, UUIDv4 } from "@libs/domain";
import { TodoWriteRepository } from "modules/todo/repository/TodoWriteRepository";
import { CompleteTodoCommand } from "../../commands/CompleteTodoCommand";

type CompleteTodoCommandResult = Result<
	void,
	DomainError[] | TodoNotFoundError
>;

export class CompleteTodoCommandHandler
	implements CommandHandler<CompleteTodoCommand, CompleteTodoCommandResult>
{
	constructor(private readonly repo: TodoWriteRepository) {}

	async handle(
		command: CompleteTodoCommand,
	): Promise<CompleteTodoCommandResult> {
		const todoId = new UUIDv4(command.todoId);
		const todoOrNone = await this.repo.getById(todoId);
		if (todoOrNone.isNone()) {
			return Err(new TodoNotFoundError(command.todoId));
		}

		const todo = todoOrNone.unwrap();

		const result = todo.complete();
		if (result.isErr()) {
			return result;
		}

		await this.repo.save(todo);

		return Ok(undefined);
	}
}
