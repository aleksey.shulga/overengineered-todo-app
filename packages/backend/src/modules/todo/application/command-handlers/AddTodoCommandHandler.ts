import { DomainError, UUIDv4 } from "@libs/domain";
import { AddTodoCommand } from "../../commands/AddTodoCommand";

import { CommandHandler } from "@libs/commands";
import { TitleValueObject } from "modules/todo/domain/TitleValueObject";
import { TodoEntity } from "modules/todo/domain/TodoEntity";
import { TodoWriteRepository } from "modules/todo/repository/TodoWriteRepository";
import { Ok, Result } from "oxide.ts";

export type AddTodoCommandResult = Result<string, DomainError[]>;

export class AddTodoCommandHandler
	implements CommandHandler<AddTodoCommand, AddTodoCommandResult>
{
	constructor(private readonly repo: TodoWriteRepository) {}

	async handle(command: AddTodoCommand): Promise<AddTodoCommandResult> {
		const titleOrErr = TitleValueObject.create(command.title);
		if (titleOrErr.isErr()) {
			return titleOrErr;
		}

		const userId = new UUIDv4(command.userId);
		const title = titleOrErr.unwrap();
		const todo = TodoEntity.create({ userId, title, completed: false });
		await this.repo.save(todo);
		return Ok(todo.id.toString());
	}
}
