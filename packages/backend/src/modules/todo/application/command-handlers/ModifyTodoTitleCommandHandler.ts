import { CommandHandler } from "@libs/commands";
import { DomainError, UUIDv4 } from "@libs/domain";
import { TitleValueObject } from "modules/todo/domain/TitleValueObject";
import { TodoNotFoundError } from "modules/todo/errors/TodoNotFoundError";
import { TodoWriteRepository } from "modules/todo/repository/TodoWriteRepository";
import { Err, Ok, Result } from "oxide.ts";
import { ModifyTodoTitleCommand } from "../../commands/ModifyTodoTitleCommand";

type ModifyTodoTitleCommandResult = Result<
	void,
	DomainError[] | TodoNotFoundError
>;

export class ModifyTodoTitleCommandHandler
	implements
		CommandHandler<ModifyTodoTitleCommand, ModifyTodoTitleCommandResult>
{
	constructor(private readonly repo: TodoWriteRepository) {}

	public async handle(
		command: ModifyTodoTitleCommand,
	): Promise<ModifyTodoTitleCommandResult> {
		const titleOrErr = TitleValueObject.create(command.title);
		if (titleOrErr.isErr()) {
			return titleOrErr;
		}
		const title = titleOrErr.unwrap();
		const todoId = new UUIDv4(command.todoId);
		const todoOrNone = await this.repo.getById(todoId);
		if (todoOrNone.isNone()) {
			return Err(new TodoNotFoundError(command.todoId));
		}
		const todo = todoOrNone.unwrap();

		const result = todo.changeTitle(title);
		if (result.isErr()) {
			return result;
		}
		await this.repo.save(todo);

		return Ok(undefined);
	}
}
