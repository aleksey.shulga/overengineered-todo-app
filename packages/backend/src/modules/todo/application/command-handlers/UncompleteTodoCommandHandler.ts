import { CommandHandler } from "@libs/commands";
import { DomainError, UUIDv4 } from "@libs/domain";
import { TodoNotFoundError } from "modules/todo/errors/TodoNotFoundError";
import { TodoWriteRepository } from "modules/todo/repository/TodoWriteRepository";
import { Err, Ok, Result } from "oxide.ts";
import { UncompleteTodoCommand } from "../../commands/UncompleteTodoCommand";

type UncompleteTodoCommandResult = Result<
	void,
	DomainError[] | TodoNotFoundError
>;

export class UncompleteTodoCommandHandler
	implements CommandHandler<UncompleteTodoCommand, UncompleteTodoCommandResult>
{
	constructor(private readonly repo: TodoWriteRepository) {}

	async handle(
		command: UncompleteTodoCommand,
	): Promise<UncompleteTodoCommandResult> {
		const todoId = new UUIDv4(command.todoId);
		const todoOrNone = await this.repo.getById(todoId);
		if (todoOrNone.isNone()) {
			return Err(new TodoNotFoundError(command.todoId));
		}

		const todo = todoOrNone.unwrap();

		const result = todo.uncomplete();
		if (result.isErr()) {
			return result;
		}

		await this.repo.save(todo);

		return Ok(undefined);
	}
}
