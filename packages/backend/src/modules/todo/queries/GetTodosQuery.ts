import { Query, QueryProps } from "@libs/queries";

export class GetTodosQuery extends Query {
	readonly userId: string;

	constructor(props: QueryProps<GetTodosQuery>) {
		super();
		this.userId = props.userId;
	}
}
