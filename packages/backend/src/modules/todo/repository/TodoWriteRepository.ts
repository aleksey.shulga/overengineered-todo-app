import { UUIDv4 } from "@libs/domain";
import { ConcurrencyError, EventStore } from "@libs/eventstore";
import { WriteRepository } from "@libs/repository";
import { TodoEntity } from "modules/todo/domain/TodoEntity";
import { None, Option, Result, Some } from "oxide.ts/core";

export class TodoWriteRepository implements WriteRepository<TodoEntity> {
	constructor(private eventStore: EventStore) {}

	async getById(id: UUIDv4): Promise<Option<TodoEntity>> {
		const streamName = this.getStreamName(id);
		const events = await this.eventStore.read(streamName);
		if (events.length === 0) {
			return None;
		}
		const todo = TodoEntity.fromEvents(events);
		if (todo.deleted) {
			return None;
		}
		return Some(todo);
	}

	async save(todo: TodoEntity): Promise<Result<void, ConcurrencyError>> {
		const events = todo.pullEvents();
		const srteamName = this.getStreamName(todo.id);
		return await this.eventStore.save(srteamName, events, todo.getVersion());
	}

	private getStreamName(id: UUIDv4): string {
		return `todo-${id}`;
	}
}
