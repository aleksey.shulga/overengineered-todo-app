import { ReadRepository } from "@libs/repository";
import { TodoModel } from "modules/todo/models/TodoModel";
import { Option } from "oxide.ts/core";

export class TodoReadRepository implements ReadRepository<TodoModel> {
	async getById(id: string): Promise<Option<TodoModel>> {
		throw new Error("Method not implemented.");
	}
	async getAll(): Promise<TodoModel[]> {
		throw new Error("Method not implemented.");
	}
}
