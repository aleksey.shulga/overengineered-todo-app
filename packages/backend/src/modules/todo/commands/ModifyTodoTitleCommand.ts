import { Command } from "@libs/commands";

export class ModifyTodoTitleCommand extends Command {
	readonly todoId: string;
	readonly title: string;

	constructor(todoId: string, title: string) {
		super();
		this.todoId = todoId;
		this.title = title;
	}
}
