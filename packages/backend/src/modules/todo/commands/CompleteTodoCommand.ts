import { Command } from "@libs/commands";

export class CompleteTodoCommand extends Command {
	readonly todoId: string;
	constructor(todoId: string) {
		super();
		this.todoId = todoId;
	}
}
