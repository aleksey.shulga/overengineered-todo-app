import { Command } from "@libs/commands";

export class UncompleteTodoCommand extends Command {
	readonly todoId: string;
	constructor(todoId: string) {
		super();
		this.todoId = todoId;
	}
}
