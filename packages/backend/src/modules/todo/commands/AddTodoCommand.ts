import { Command, CommandProps } from "@libs/commands";

export class AddTodoCommand extends Command {
	readonly title: string;
	readonly userId: string;

	constructor(props: CommandProps<AddTodoCommand>) {
		super();
		this.title = props.title;
		this.userId = props.userId;
	}
}
