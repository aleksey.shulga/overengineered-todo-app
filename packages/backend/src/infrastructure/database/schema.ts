import {
	boolean,
	integer,
	pgTable,
	serial,
	text,
	varchar,
} from "drizzle-orm/pg-core";

export const users = pgTable("users", {
	id: serial("id").primaryKey(),
	username: varchar("username", { length: 255 }).notNull().unique(),
	password: text("password").notNull(),
});

export const todos = pgTable("todos", {
	id: serial("id").primaryKey(),
	userId: integer("user_id").references(() => users.id),
	title: varchar("title", { length: 255 }).notNull(),
	completed: boolean("completed").default(false),
});
